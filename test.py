# -------
# imports
# -------

import os
import sys
import unittest
from models import db, Stocks, Sectors, Indices, Industries

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------
    
    def test_source_insert_1(self):
        s = Stocks(sym = 'AAPL', name = 'Apple', country = 'US', sector = 'Information Technology', industry = 'Technology', index = 'SP500')
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Stocks).filter_by(sym = 'AAPL').one()
        self.assertEqual(str(r.sym), 'AAPL')

        db.session.query(Stocks).filter_by(sym = 'APPL').delete()
        db.session.commit()
    
    def test_source_insert_2(self):
        s = Sectors(name = "Anime")
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Sectors).filter_by(name = 'Anime').one()
        self.assertEqual(str(r.name), 'Anime')

        db.session.query(Sectors).filter_by(name = 'Anime').delete()
        db.session.commit()
    
    def test_source_insert_3(self):
        s = Industries( name = "Coke", sector_name = "Diet")
        db.session.add(s)
        db.session.commit()


        r = db.session.query(Industries).filter_by(name = 'Coke').one()
        self.assertEqual(str(r.name), 'Coke')

        db.session.query(Industries).filter_by(name = 'Coke').delete()
        db.session.commit()
    
    
    
# def main():
#     db.session.query(Stocks).filter_by(sym = 'APPL').delete()
#     db.session.commit()

# main()

if __name__ == '__main__':
    unittest.main()