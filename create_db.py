#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/create_db.py
# Fares Fraij
# adapted for use with project 1
# ---------------------------

import json
from models import app, db, stocks, sectors, indices, industries

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create_stocks
# ------------
def create_tables():
    """
    populate stocks, sectors, and industries table from alphavantage jsons

    """
    
    stock = load_json('stocks_test.json')
    sector_industry = {}

    # unique id counter 
    # stock_id = 1
    # to update: take the length of # of objects in the stock table and continue the cntr from there 

    # iterate through each stock json object 
    for st in stock['stocks']:
        stock_sym = st["Symbol"]
        stock_name = st['Name']
        country = st["Country"]

        stock_index = st["Exchange"]
        stock_sect = st["Sector"]
        stock_indu = st["Industry"]

        # new stock object
        newStock = stocks( sym = stock_sym, name = stock_name, country = country, sector = stock_sect, industry = stock_indu, index = stock_index)
        db.session.add(newStock)
        print("Stock: " + stock_sym + " added")
        #make a catalog of all of the sectors and the corresponding industries
        if stock_sect not in sector_industry: 
            sector_industry[stock_sect] = []
            
            # new sector object
            newSector = sectors( name = stock_sect)
            db.session.add(newSector)
            print("Sector: " + stock_sect + " added")

        stock_sect = st["Sector"]
        if stock_indu not in sector_industry[stock_sect]: 
            sector_industry[stock_sect].append(stock_indu)
            
            # new sector object
            newIndustry = industries( name = stock_indu, sector_name = stock_sect)
            db.session.add(newIndustry)
            print("Industry: " + stock_indu + " added")

        #stock_id += 1
        # After I create the book, I can then add it to my session. 
        
        # commit the session to my DB.
        db.session.commit()
'''        
    print("Dictionary of sectors : industries")
    for sector in sector_industry: 
        print(sector + ": ", end = "")
        print(sector_industry[sector])
'''

#db.drop_all()
#db.create_all()

#create_tables()
