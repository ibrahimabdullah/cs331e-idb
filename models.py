#!/usr/bin/env python3

# ---------------------------
# projects/IDB3/models.py
# Fares Fraij
# adapted for project 1 use
# ---------------------------

from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS


# not sure about the build/static for us
app = Flask(__name__, static_folder="./static", template_folder="./templates")
CORS(app)
#might need to change the last postgres to market-db
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://postgres:diamondhands@34.69.76.52/market') 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

# ------------
# Market-DB Tables Schema 
# ------------


# ------------
# M:M relationships
# ------------

'''
# stock-indicies
link = db.Table('link',
   db.Column('author_id', db.Integer, db.ForeignKey('author.id')), 
   db.Column('book_id', db.Integer, db.ForeignKey('book.id'))
   )

# indices-sectors
'''

'''
class names need to be all lowercase 
------------
Stocks Table Schema 
------------
'''
class stocks(db.Model):
    """
    Stock class has 6 attributes
    symbol -> the key 
    comp_name 
    year
    country
    reddit_count
    # description would take too much space
    """
    __tablename__ = 'stocks'

    sym = db.Column(db.String(8), primary_key = True)
    name = db.Column(db.String(80), nullable = True)
    country = db.Column(db.String(40), nullable = True)
    reddit_count = db.Column(db.Integer, nullable = True)
    #description = db.Column(db.String(40), nullable = True)

    # a stock can be apart of many indices, an indice can have many stocks M:M relationship
    #index = db.relationship('Indices', backref = 'stocks')
     
    index = db.Column(db.String(80), nullable = True)

    # a stock has one sector, a sector has many stocks in it, 1:M relationship
    #sector_name = db.Column(db.String(25), db.ForeignKey('sectors.name'))
    sector = db.Column(db.String(40), nullable = True)

    # a stock has one industry, an industry has many stocks in it, 1:M relationship 
    #indu_name = db.Column(db.Integer, db.ForeignKey('industries.name'))
    industry = db.Column(db.String(40), nullable = True)

'''
------------
Sectors Tables Schema 
------------
'''
class sectors(db.Model):
    """
    Sector class has 4 attributes
    sector_name -> key
    cap 
    """
    __tablename__ = 'sectors'

    #id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(40), primary_key = True)
    type = db.Column(db.String(40), nullable = True)
    percent = db.Column(db.String(25), nullable = True)
    cap = db.Column(db.Integer, nullable = True)  
    one_yr_ch = db.Column(db.String(25), nullable = True)

    # stock-sector relationship, 1:M 
    #stocks = db.relationship('Stocks', backref = 'sectors')

    # a sector has many industries , 1:M relationship 
    #industries = db.relationship('Industries', backref = 'sectors')

    # sector-indice relationship M:M


 
'''
------------
Indices Tables Schema 
------------
'''
class indices(db.Model):
    """
    Index class has 5 attributes
    index_id -> key 
    index_name
    year
    i_comp - # of companies
    cap
    
    """
    __tablename__ = 'indices'

    sym = db.Column(db.String(8), primary_key = True)
    name = db.Column(db.String(25), nullable = True)
    year = db.Column(db.Integer, nullable = True)
    i_comp = db.Column(db.Integer, nullable = True)
    cap = db.Column(db.Integer, nullable = True)   

    # stock-index relation 1:M
    #stock_sym = db.Column(db.String(8), db.ForeignKey('stocks.sym'))


# ------------
# Industries Tables Schema 
# ------------
class industries(db.Model):
    """
    Industry class has attributes 
    indu_id -> key
    indu_name 
    """
    __tablename__ = 'industries'

    #id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(40), primary_key = True)

    # stock-industry relationship, 1:M
    #stocks = db.relationship('Stocks', backref = 'industries')

    # a industry has one sector, sector has many industries , 1:M relationship 
    #sector_name = db.Column(db.String(25), db.ForeignKey('sectors.name'))
    sector_name = db.Column(db.String(40), nullable = True)



"""
Relations:
    Stock_Index 
    symbol, index_id

    Stock_Sector
    symbol, sector_id

    Sector_Index
    sector_id, index_id

    
"""
'''
class rSectorIndustry(db.Model):
    
    Sector_Industry
    sector_name, industry_name

    
    indu_name = db.Column(db.String(25), nullable = False)
    sector_name = db.Column(db.String(25), nullable = False)
'''
