import json
from new_models import app, db, Stock, Sector,Industry

# ------------
# load_json
# ------------
def load_json(filename):

    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_tables():
    temp1 = 0
    temp2 = 0
    in_list = []
    sec_list = []
    sector = load_json('json_files/sectors.json')
    industry = load_json('json_files/industries.json')
    stock = load_json('json_files/stocks.json')

    for i in sector['sectorPerformance']:
        s_n = i['sector']
        p = i['changesPercentage']
        ind = i['Largest Cap Stocks']
        c = i['Market Cap']
        yr = i['1-Year % Change']
        new_sec = Sector(s_name = s_n , percent = p , index = ind , cap = c, one_yr_ch = yr )
        db.session.add(new_sec)
        sec_list.append(new_sec)
        db.session.commit()
    
    for x in industry['industryPerformance']:
        i_n = x['industry']
        p = x['changesPercentage']
        ind = x['Largest Cap Stocks']
        c = x['Market Cap']
        yr = x['1-Year % Change']

        for obj in sec_list:
            if obj.s_name == x['sector']:
                temp1 = obj
                break
        
        new_ind = Industry(i_name =i_n , percent = p, index = ind , cap = c, one_yr_ch =yr,sectors = temp1 )
        db.session.add(new_ind)
        in_list.append(new_ind)
        db.session.commit()

    for st in stock['Stocks']:
        stock_sym = st["sym"]
        stock_name = st['name']
        countr = st["country"]
        stock_index = st["index"]
        red = st['reddit_count']

        for obj1 in sec_list:
            if obj.s_name == st['sector']:
                temp1 = obj1
                break

        for obj2 in in_list:
            if obj2.i_name == st["industry"]:
                temp2 = obj2
                break

        new_stoc = Stock(sym = stock_sym, name =stock_name, country =countr, index = stock_index,reddit_count = red,sectors =obj1 ,industries = obj2)
        db.session.add(new_stoc)
        db.session.commit()

# create_tables()