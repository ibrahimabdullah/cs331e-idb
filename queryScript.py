# script for querying the vantage point api for the top 50 stocks in every sector 

'''
outline

- list of top stocks in each sector (20ish)
- query api at 5/min, max of 500 per day
- goal is to have 25 stocks per sector (11 sectors minimum)
- 1 query / 15 seconds 

- take list
- query for company info
- save only relevent info in json (already in json form so just saving the desired attributes)
- sleep for 15 seconds
- counter for # of queries to never exceed 500 queries/day
- counter for # of queries to never exceed 5 per minute
- for loop through list and save into output json file


- in the future, take the reddit tickers and if not in database, then query for them 
sectors: Energy, Industrial, Consumer Discretionary, Materials, Financial, Tech, Health, Consumer Services, Consumer Staples, Real Estate, Utilities

stock attributes- stock 
gcp db market-db
user: postgres
pass: diamondhands
'''
import json
import time 
import requests
from models import app, db, stocks, sectors, indices, industries

# ------------
# to query 
# ------------
#indices = ["XLK", "NASDAQ", "IXIC", "DJI", "SPX", "VIX" ]
'''
stock= [ "ENPH", "HUBS", "BILL", "TTD", "EQIX", "CCI", "AMT", 
        "QCOM", "MU", "FCX", "DOW", "LYB", "EL", "CVX", "XOM", 
        "COP", "WFC", "COF", "AXP", "BSX", "PFE", "ABT", "GE", 
        "BA", "DE", "TWTR", "NFLX", "CHTR", "BKNG", "NKE", "TSLA", 
        "DIS", "WMT", "HLT","ALL","HCA", "ALK", "QRVO", "BP",
        "VALE", "O", "NEE", "SNDL", "TCNNF", "GME", "SKLZ", "KOSS",
        "CRSR", "COST", "CAL", "QRTEA", "AVGO", "IFF", "EXPE", "CHH",
        "IHG", "ATVI", "HON", "IIPR", "TCNNF", "XERS", "PENN", "DKNG",
        "HRTX", "TBPH", "LITE", "MAXR", "ABBV", "EAF", "AL", "HII", 
        "IR", "EFX", "UHAL", "URI", "GNRC", "CARR","RSDA", "KMI", "SLB",
        "HAL", "DD", "ECL", "VVV", "SMG", "SHW", "MMM", "UPS", "DAL",
        "LMT", "CAT", "HD", "FD", "F", "SBUX", "TGT", "CMG", "MCD", "KO",
        "PG", "KHC", "JNJ", "MRK", "MDT", "UNH", "JPM", "BAC", "USB",
        "GS", "INTC", "V", "MA", "ADBE", "CRM", "VZ", "T", "TMUS",  "CHTR", "SWI", "AZPN"]
'''
stock = []

def queryAlphaVantage(stock):
    alphaV_key = '20RLGUB6GLH6YSE0' #asya's key; need to hide this; limited to 5 querys every 5 mins 
    url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol='+ str(stock) + '&apikey='+ alphaV_key
    response = requests.get(url)
    if response: #status_code == 200:
        print("status code: " + str(response.status_code))
        return response.json()

    else: # status_code == 404:
        print(stock + ' not Found. status code: ' + str(response.status_code))
        return 0 

def create_object(st):
    # iterate through each stock json object

    stock_sym = st["Symbol"]
    # check for existing item 
    q = db.session.query(stocks).filter(stocks.sym == stock_sym).all()
    if len(q) == 0:
        stock_name = st['Name']
        country = st["Country"]

        stock_index = st["Exchange"]
        stock_sect = st["Sector"]
        stock_indu = st["Industry"]

        # new stock object
        newStock = stocks( sym = stock_sym, name = stock_name, country = country, sector = stock_sect, industry = stock_indu, index = stock_index)
        db.session.add(newStock)
        print("Stock: " + stock_sym + " added")

    # make a catalog of all of the sectors and the corresponding industries
        q = db.session.query(sectors).filter(sectors.name == stock_sect).all()
        if len(q) == 0:
            # new sector object
            newSector = sectors( name = stock_sect)
            db.session.add(newSector)
            print("Sector: " + stock_sect + " added")

        # checking industry
        q = db.session.query(industries).filter(industries.name == stock_indu).all()
        if len(q) == 0: 
            # new sector object
            newIndustry = industries( name = stock_indu, sector_name = stock_sect)
            db.session.add(newIndustry)
            print("Industry: " + stock_indu + " added")

    # commit the session to my DB.
    db.session.commit()

'''
def populatestocksDB(stock_list):
    # loop through each stock 
    # query
    # save relevant info to json/dict
    # wait 15 seconds

    for stock in stock_list:
        print("querying "+ stock)
        stock_json = queryAlphaVantage(stock)
        if stock_json != 0 :
            print("creating instance of "+ stock)
            create_object(stock_json)
            print("waiting 15 seconds")
        time.sleep(15)
    print("Done")
'''

print("starting query")

#populatestocksDB(stock_small)

# loop through each stock 
# query
# save relevant info to json/dict
# wait 15 seconds

for sto in stock:
    print("querying "+ sto)
    stock_json = queryAlphaVantage(sto)
    if stock_json != 0 :
        if len(stock_json) > 0 :
            print("creating instance of "+ sto)
            create_object(stock_json)
        else: 
            print("no instance of "+ sto + " from query")
        print("waiting 15 seconds")
    time.sleep(15)

print("Done")



'''
# json dump 
#   input: stock_id / stock symbol
#   returns: [status code 200] json  
@app.route('/stocks/company_overview_json/<str: stock_id>/')
def companyOverviewJson(stock_id):
    # check if it is zero for failed response
    return json.dumps( queryCompanyOverview(stock_id) )

'''

'''
# might not need 
# process the json? 
#   input: stock_id / stock symbol
#   returns: [status code 200] json 
@app.route('/stocks/company_overview/<str: stock_id>/')
def companyOverview(stock_id):
    stock_json = queryCompanyOverview(stock_id)
    # check if it is zero for failed response
    #return render_template()
'''