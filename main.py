# main.py
# last edited: 04/02/2021

import os
from flask import Flask, render_template, request, redirect, url_for, jsonify, send_from_directory
from new_db import app, db, create_tables, Stock,Sector,Industry
import random
import requests
import json

# app = Flask(__name__, static_folder="./static", template_folder="./templates")

###########################################
# basic pages 
###########################################

# splash/home page
@app.route('/')
#@app.route('/enterAnotherNameHere?/')
def splashPage():
    return render_template("splash.html")

###########################################

# about page
@app.route('/about/')
def aboutPage():
    return render_template("about.html")

# unittest page page
@app.route('/test/')
def testPage():
    return render_template("unittest.html")

###########################################
# develop later on 
'''
# contact page
@app.route('/contact/')
def contactPage():
    return render_template("contact.html")

'''
###########################################
# model list generation 
###########################################

# tickers main page
@app.route('/stock/',methods = ['GET','POST'],defaults = {"page":1})
@app.route('/stock/page<int:page>',methods = ['GET','POST'])
# @app.route('/stock/')
def showTickers(page = 1):
    # query table of all tickers 
    tickers_info = db.session.query(Stock).all()
    page_ticker = Stock.query.paginate(page,10,False)
    if len(tickers_info) == 0: 
        # well thats not supposed to happen!
        print("empty list")
    return render_template("tickers-model.html", tickers_info = page_ticker)

###########################################

# Market Sector 
#    overarching catagory and subsectors 
# needs work
# api aplha vantage, maybe also 
#   https://www.barchart.com/stocks/sectors/rankings
#   https://www.cloudquote.io/products/api/barchart-getsectors
# idea is to generate a list/catalog of stocks in that sector and subsector?
# start with a select few and then broaden to incude pages of stocks with search options?
#  
@app.route('/sector/',methods = ['GET','POST'],defaults = {"page":1})
@app.route('/sector/page<int:page>',methods = ['GET','POST'])
# @app.route('/sector/')
def showSectors(page = 1):
    # query db
    sectors_info = db.session.query(Sector).all()
    page_sector = Sector.query.paginate(page,5,False)
    return render_template("sectors-model.html", sectors_info = page_sector)

###########################################

# Index/indices 
#   provide list of index's and the general info plus a list of the stocks that make it up  
# needs work
# api aplha vantage, maybe also others 
# start with a select few and then broaden to incude pages of stocks with search options?
#  

# currently rerouted 
'''
@app.route('/index/')
def showIndices():
    # query db 
    indices_info = db.session.query(indices).all()

    index_stocks_dict = {}

    if len(indices_info) == 0: 
        # get list of tickers from external file 
        print("nothing here yet!")
        # query for files
    else: 
        for index in indices_info:       
            # query for stocks with matching indexes 
            index_stocks_dict[index.sym] = db.session.query(stocks).filter(stocks.index == index.sym).all()
        # will need information for the sectors

    return render_template("index-model.html", index_info = indices_info, index_stocks = index_stocks_dict)
'''
###########################################
# Industry 

#replaced indices with industry for now 
@app.route('/industry/',methods = ['GET','POST'],defaults = {"page":1})
@app.route('/industry/page<int:page>',methods = ['GET','POST'])
# @app.route('/index/')
# @app.route('/industry/')
def showIndices(page=1):
    # query db 
    industry_info = db.session.query(Industry).all()
    industry_page = Industry.query.paginate(page,5,False)
    #dictionary to hold stock queries
    # industry_stock_dict = {}

    # if len(industry_info) == 0: 
    #     # get list of tickers from external file 
    #     print("empty list")
    #     # query for files
    # else: 
    #     for industry in industry_info:       
    #         # query for stocks with matching industries 
    #         industry_stock_dict[industry.name] = db.session.query(Stock).filter(Stock.industry == industry.name).all()
 

    return render_template("industry-model.html", industry_info = industry_page)


###########################################
# instance list generation 
###########################################

@app.route('/stock/<stock>/')
def tickerInstance(stock):
    # query stock
    ticker_row = db.session.query(Stock).filter(Stock.sym == stock).add_columns(Stock.sym, Stock.name, Stock.country, Stock.reddit_count, Stock.index, Stock.ind_name, Stock.sector_name).all()
    return render_template("ticker-instance.html", stock_info = ticker_row)

###########################################

@app.route('/sector/<sector>/')
def sectorInstance(sector):
    """
    sectors.name
    type
    percent
    cap
    one year change

    industries
    stocks
    indices (later)
    """
    sector_row = db.session.query(Sector).filter(Sector.s_name == sector).add_columns(Sector.s_name, Sector.index, Sector.percent,Sector.cap, Sector.one_yr_ch,Sector.stocks,Sector.industries).all()
    
    # query for industries with matching sector 
    # sector_industry = db.session.query(Industry).filter(Industry.sector_name == sector).all()
    # sector_stocks = db.session.query(Stock).filter(Stock.sector == sector).all()

    #sector_row.append(sector_industry)
    #sector_row.append(sector_stocks)
    
    return render_template("sector-instance.html", sector_info= sector_row)

###########################################
# index instance 
# rerouted for now to industries
'''
@app.route('/index/<index>/')
def indexInstance(index):
    index_row =  db.session.query(indices).filter(indices.sym == index).all()
    return render_template("index-instance.html", index_info = index_row)
'''
###########################################
# industry instance 
@app.route('/index/<index>')
@app.route('/industry/<index>')
def indexInstance(index):
    industry_row =db.session.query(Industry).filter(Industry.i_name == index).add_columns(Industry.i_name,Industry.percent,Industry.index,Industry.cap,Industry.one_yr_ch,Industry.stocks,Industry.sector_name).all()
    #dictionary to hold stock queries
    # industry_stock_dict = {}

    # if len(industry_row) == 0: 
    #     # get list of tickers from external file 
    #     print("empty list")
    #     # query for files
    # else: 
    #     for industry in industry_row:       
    #         # query for stocks with matching industries 
    #         industry_stock_dict[industry.name] = db.session.query(Stock).filter(Stock.industry == industry.name).all()
 

    # return render_template("industry-model.html", industry_info = industry_row, industry_stocks = industry_stock_dict)

    return render_template("index-instance.html", industry_info = industry_row)

###########################################


###########################################
# commented out code and ideas for other functions 
###########################################

# general Stock/Company Overview 
# api alphaVantage 

# query api 
#   input: stock_id / stock symbol
#   returns: [status code 200] json
'''
def queryCompanyOverview(stock_id):
    alphaV_key = '20RLGUB6GLH6YSE0' #asya's key; need to hide this; limited to 5 querys every 5 mins 
    url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol='+ str(stock_id) + '&apikey='+ alphaV_key
    response = requests.get(url)
    if response: #status_code == 200:
        print("status code: " + str(response.status_code))
        r = response.json()
        # query json for relevent info 
        return r
    else: # status_code == 404:
        print('Not Found. status code: ' + str(response.status_code))
        return 0 
'''
'''
# json dump 
#   input: stock_id / stock symbol
#   returns: [status code 200] json  
@app.route('/stocks/company_overview_json/<str: stock_id>/')
def companyOverviewJson(stock_id):
    # check if it is zero for failed response
    return json.dumps( queryCompanyOverview(stock_id) )

'''
'''
# might not need 
# process the json? 
#   input: stock_id / stock symbol
#   returns: [status code 200] json 
@app.route('/stocks/company_overview/<str: stock_id>/')
def companyOverview(stock_id):
    stock_json = queryCompanyOverview(stock_id)
    # check if it is zero for failed response
    #return render_template()
'''

# basic code idea for adding values into a db from api query
'''
for ticker in tickers: 
    curr_ticker = { 'stock_id': ticker}
    # query alpha vantage for company, sector, etc.
    # alt. use companyOverview(ticker)
    info = queryCompanyOverview(ticker) #ticker should match stock symbol?
    # alt. combine dicts?

    for i in info: 
        curr_ticker[] = i   

    # cross reference if on the index? (need to work on this)
    # save things into a list of dictionaries [{c0::c4},...]
    # c0: ticker/stock name ; c1: Company Name; c2: sector ; c3: index (N/A) if not; c4: ;
    tickers_info.append()
'''




# helpful template, not apart of this project
# delete later 

'''
# model/column 1 page
@app.route('/stocks/tickers/')
def renderTickers(book_id):
    if request.method == 'POST':
        for book in books:
            if book['id'] == str(book_id):
                books.remove(book)
        return redirect(url_for('showBook')) 
    else:
        book_title=""
        for book in books: 
            if book['id'] == str(book_id):
                book_title = book['title']
        return render_template("delete.html", book_id = book_id, book_title = book_title)
'''

'''
# database test
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
  return render_template("index.html")

@app.route("/api/test")
def test():
      return "TEST123456789"

@app.route("/api")
def test1():
      return "Welcome to this web API!"

@app.route('/api/books')
def book():
	book_list = db.session.query(Book).all()
	print(book_list)
	return str([book.title for book in book_list])
'''

'''
# a list of dictionaries used for the static phase 
# c0: ticker/stock name ; c1: Company Name; c2: sector ; c3: index (N/A) if not; c4: ;

# debugging test ; made up stuff 
tickers_info = [ 
                {'stock': 'APPL', 'company': 'Apple', 'sector': 'Technology', 'index': ['SPX'], 'year': '1980','country':'USA'},
                {'stock': 'UNH', 'company':'United HealthCare', 'sector': 'Medical', 'index': ['DJIA'], 'year': '1984','country':'USA'},
                {'stock': 'MSFT', 'company': 'Microsoft', 'sector': 'Technology', 'index': ['SPX'], 'year': '1986', 'country':'USA'},
                {'stock': 'TSLA', 'company': 'Tesla', 'sector': 'Automotives', 'index': ['COMP'], 'year': '2003', 'country':'USA'}
               ]
# needs work
sectors_info = [ 
                {'sector': 'Technology', 'stock': 'APPL',  'index': 'SPX', 'cap':'$9.1T', 'type':'Sensitive Super', "beta": "0.97"},
                {'sector': 'Medical', 'stock': 'UNH',  'index': 'DJIA', 'cap':'$1.27T', "type":"Defensive","beta": "0.74"},
                {'sector': 'Automotives','stock': 'TSLA',  'index': 'COMP', 'cap':'8.45T', "type":"Cyclical","beta": "1.05"}
                ] 
# needs work 
indices_info = [ 
                {'index': 'SPX', 'name':'S&P 500', 'stock':'APPL','sector': 'Multiple', 'companies': '500', 'year':'1957', 'cap':'$31.61T' },
                {'index': 'COMP', 'name':'NASDAQ Composite Index', 'stock':'APPL', 'sector': 'Multiple', 'companies': '2,500','year':'1971', 'cap':'$22.72B' },
                {'index': 'DJIA', 'name':'Dow Jones Industrial Average', 'stock':'UNH', 'sector': 'Multiple', 'companies': '30','year':'1896', 'cap':'$8.33T' },
               ] 
'''


if __name__ == '__main__':
    #app.debug = True
    #app.run(host = '0.0.0.0', port = 5000)
    app.run(host = 'localhost', port = 5000,debug=True)

