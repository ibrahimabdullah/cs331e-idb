from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
import os
from flask_cors import CORS

app = Flask(__name__, static_folder="./static", template_folder="./templates")
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgresql://postgres:diamondhands@34.69.76.52/markettrendies') 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

#a sector have many stocks but a stock only have one sector
#a sector have many industries but a industry only have one sector 
#a industry have many stocks but a stock only have one industry

class Sector(db.Model):
    __tablename__ = 'sectors'
	
    s_name = db.Column(db.String(40), primary_key = True)
    percent = db.Column(db.String(25), nullable = True)
    index = db.Column(db.String(40), nullable = True)
    cap = db.Column(db.String(40), nullable = True)
    one_yr_ch = db.Column(db.String(25), nullable = True)

    # connect to stock and industries
    stocks = db.relationship('Stock', backref = 'sectors')
    industries = db.relationship('Industry', backref = 'sectors')

class Industry(db.Model):
    __tablename__ = 'industries'
	
    i_name = db.Column(db.String(40), primary_key = True)
    percent = db.Column(db.String(25), nullable = True)
    index = db.Column(db.String(40), nullable = True)
    cap = db.Column(db.String(40), nullable = True)
    one_yr_ch = db.Column(db.String(25), nullable = True)
    
    #connect to stocks
    stocks =  db.relationship('Stock', backref = 'industries')
    #property for sector
    sector_name = db.Column(db.String(40), db.ForeignKey('sectors.s_name'))

class Stock(db.Model):
    __tablename__ = 'stocks'
	
    sym = db.Column(db.String(8), primary_key = True)
    name = db.Column(db.String(80), nullable = True)
    country = db.Column(db.String(40), nullable = True)
    index = db.Column(db.String(40), nullable = True)
    reddit_count = db.Column(db.Integer, nullable = True)

    #property for sector
    sector_name = db.Column(db.String(40), db.ForeignKey('sectors.s_name'))
    #property for industry
    ind_name = db.Column(db.String(40), db.ForeignKey('industries.i_name'))

# db.drop_all()
# db.create_all()